<?php
/**
 * @author dmz9 <dmz9@yandex.ru>
 * @copyright 2017 http://ipolh.com
 * @licence MIT
 */
namespace WPWooCommerceDDelivery;

require_once $_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/woocommerce_ddelivery/vendor/DDeliveryWidgetApi.php";

class Controller {
	public static function actionDDelivery() {

		$container = Helper::createContainer();
		$container->getUi()
		          ->render( $_REQUEST );
		die();
	}

	public static function actionSDKToken() {

		$products = ( isset( $_POST ) && isset( $_POST['products'] ) )
			? $_POST['products']
			: array();
		$discount = ( isset( $_POST ) && isset( $_POST['discount'] ) )
			? $_POST['discount']
			: array();

		$container       = Helper::createContainer( array(
			                                            'form'     => $products,
			                                            'discount' => (float) $discount
		                                            ) );
		$business        = $container->getBusiness();
		$cartAndDiscount = $container->getAdapter()
		                             ->getCartAndDiscount();
		$token           = $business->renderModuleToken( $cartAndDiscount );

		return array(
			'url' => $container->getAdapter()
			                   ->getSdkServer() . 'delivery/' . $token . '/index.json'
		);
	}

	public static function actionUserCart() {
		return Helper::getCurrentCartProducts();
	}

	/**
	 * сохраняем sdk-id выданый дделивери. это ид черновика.
	 *
	 * @return array
	 */
	public static function actionSaveSDK() {
		$session           = WC()->session;
		$field             = Core::SESSION_FIELD_SDK_ID;
		$session->{$field} = json_encode($_POST);

		$field   = Core::SESSION_FIELD_PRICE;
		unset( $session->{$field} );
		$session->{$field} = $_POST['delivery']['total_price'];
		// hack to remove session key for ddelivery shipping package to force it recalculate instead of getting cached
		// @stolen in class-wc-shipping.php@calculate_shipping_for_package()
		// emulating case when recalculating is forced
		$total = $session->get( 'shipping_method_counts', array());
		foreach ( $total as $i => $whatever ) {
			$session->set( 'shipping_for_package_' . $i,
			               'censored' );
		}

		$session->save_data();

		return array( 'status' => 'ok' );
	}

	public static function actionApiScript() {

		$adapter=new WPAdapter();
		$token=$adapter->getApiKey();

        $widgetApi = new \DDeliveryWidgetApi();

        // Передайте здесь свой API-key
        $widgetApi->setApiKey($token);

        $widgetApi->setMethod($_SERVER['REQUEST_METHOD']);
        $widgetApi->setData(isset($_REQUEST['data']) ? $_REQUEST['data'] : []);
        if (preg_match('/main\.php/isu',$_REQUEST['url'])) header('Content-type: text/html');
        if ($_REQUEST['url']=='https://ddelivery.ru/api/:key/sdk/save-order.json')
        {
        	$data=$widgetApi->submit($_REQUEST['url']);
        	$mdata=json_decode($data,true);

			$wc      = WC();
			$session = $wc->session;
			$field   = Core::SESSION_FIELD_PREV;
			unset( $session->{$field} );
			$session->{$field} = $mdata['data']['id'];
			$session->save_data();
        	echo $data;	
        }
		else echo $widgetApi->submit($_REQUEST['url']);
		die();
	}

	/**
	 * сохраняем выбраную йузером цену доставки в сессии
	 *
	 * @return array
	 */
	public static function actionSavePrice() {
		$data = $_POST['data'];
		if ( empty( $data ) ) {
			return array( 'status' => 'fail' );
		}

		$price = $data['price'];

		$wc      = WC();
		$session = $wc->session;
		$field   = Core::SESSION_FIELD_PRICE;
		unset( $session->{$field} );
		$session->{$field} = $price;
		// hack to remove session key for ddelivery shipping package to force it recalculate instead of getting cached
		// @stolen in class-wc-shipping.php@calculate_shipping_for_package()
		// emulating case when recalculating is forced
		$total = $session->get( 'shipping_method_counts', array());
		foreach ( $total as $i => $whatever ) {
			$session->set( 'shipping_for_package_' . $i,
			               'censored' );
		}

		$session->save_data();

		return array( 'status' => 'ok' );
	}

	public static function actionDebug() {
		$order = Helper::getOrder( $_POST['orderId'] );

		$order->save();

		return die( print_r( $order,
		                     1 ) );
	}

	public static function actionGetStatuses() {
		echo json_encode(wc_get_order_statuses());
		die();
	}

	public static function actionGetPayment() {
		foreach (WC()->payment_gateways->get_available_payment_gateways() as $key => $item) {
			$out[$key] = $item->title;
		}
		echo json_encode($out);
		die();
	}

	public static function actionUpdateStatus() {
		global $wpdb,$_GET;
		$order_data=$wpdb->get_results(  "UPDATE ".$wpdb->prefix."posts SET ddelivery_track_id='".addslashes($_GET['ddelivery_track_number'])."',post_status='".addslashes($_GET['status'])."' WHERE ddelivery_id=".$_GET['ddelivery_id']  );
	}

	/**
	 * backend only. sending order to ddelivery is here
	 *
	 * @param $orderId
	 *
	 * @return int
	 */
	public static function actionOrderUpdate( $orderId ) {
		global $wpdb;
		$adapter=new WPAdapter();
		$token=$adapter->getApiKey();

		$logger = new WPLogStorage();
		$logger->saveLog( " " );

		$orderId = (int) $orderId;
		if ( empty( $orderId ) ) {
			$logger->saveLog( "Order create stopped: empty orderId" );

			return $orderId;
		}

		$logger->saveLog( 'Order update hook ' . $orderId );

		$session = WC()->session;

		$container = Helper::createContainer();
		$business  = $container->getBusiness();
		try {
			$order = Helper::getOrder( $orderId );
			$order_data=$wpdb->get_results(  "SELECT * FROM ".$wpdb->prefix."posts WHERE ID=".$orderId  );
			$params=[
				'id'=>$order_data[0]->ddelivery_prev_id,
				'status'=>'wc-'.$order->get_status(),
				'cms_id'=>$orderId,
				'payment_method'=>Helper::stringToNumber( $order->get_payment_method() ),
			];

			if ($order->get_status() == 'completed') {
				$params['payment']=true;
			}
			
			if( $curl = curl_init() ) {
				curl_setopt($curl, CURLOPT_URL, 'https://ddelivery.ru/api/'.$token.'/sdk/update-order.json');
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
				$out = curl_exec($curl);
				$mout=json_decode($out,true);

				if ($mout['data']['cabinet_id']!='') $row = $wpdb->get_results(  "UPDATE ".$wpdb->prefix."posts SET ddelivery_id='".addslashes($mout['data']['cabinet_id'])."' WHERE ID=".$orderId  );
				curl_close($curl);
			}
		} catch ( \Exception $exception ) {
			$logger->saveLog( "Exception in actionOrderCreate: {$exception->getMessage()}" );

			return $orderId;
		}
	}

	/**
	 * backend only. binding order to ddelivery
	 *
	 * @param $orderId
	 *
	 * @return int
	 */
	public static function actionOrderCreate( $orderId ) {
		global $wpdb;
		$adapter=new WPAdapter();
		$token=$adapter->getApiKey();

		$logger = new WPLogStorage();
		$logger->saveLog( " " );

		$orderId = (int) $orderId;
		if ( empty( $orderId ) ) {
			$logger->saveLog( "Order create stopped: empty orderId" );

			return $orderId;
		}

		$logger->saveLog( 'Order create hook ' . $orderId );

		$session = WC()->session;

		$container = Helper::createContainer();
		$business  = $container->getBusiness();
		try {
			$order = Helper::getOrder( $orderId );
			$params=[
				'id'=>$session->{Core::SESSION_FIELD_PREV},
				'status'=>$order->get_status(),
				'cms_id'=>$orderId,
				'payment_method'=>Helper::stringToNumber( $order->get_payment_method() ),
			];

			if( $curl = curl_init() ) {
				curl_setopt($curl, CURLOPT_URL, 'https://ddelivery.ru/api/'.$token.'/sdk/update-order.json');
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
				$out = curl_exec($curl);
				$mout=json_decode($out,true);
				// print_r('https://ddelivery.ru/api/'.$token.'/sdk/update-order.json');
				// echo '<br>';
				// print_r($params);
				// echo '<br>';
				$row = $wpdb->get_results(  "UPDATE ".$wpdb->prefix."posts SET ddelivery_prev_id='".addslashes($params['id'])."' WHERE ID=".$orderId  );
				curl_close($curl);
			}
		} catch ( \Exception $exception ) {
			$logger->saveLog( "Exception in actionOrderCreate: {$exception->getMessage()}" );

			return $orderId;
		}
		unset( $session->{Core::SESSION_FIELD_PREV} );
		// print_r($orderId);
		die();

		return $orderId;
	}
}