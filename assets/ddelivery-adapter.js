(function ($) {
    $(document).ready(function () {
        if (document.getElementById('ddelivery_checker')) {
            document.getElementById('ddelivery_checker').style.width="0px";
            document.getElementById('ddelivery_checker').style.height="0px";
            document.getElementById('ddelivery_checker').style.border="0px";
        }
        function debounce (f, ms) {
            var timer = null;
            
            return function (data) {
                var _this = this;
                
                function onComplete () {
                    f.call(_this, data);
                    timer = null;
                }
                
                if (timer) clearTimeout(timer);
                
                timer = setTimeout(onComplete, ms);
            };
        }

        if (typeof woocommerce_ddelivery_settings === 'undefined') {
            return void (0);
        }
        var settings = woocommerce_ddelivery_settings;
        var controller = {
            url: '',
            $errorContainer: $('<p/>', {
                css: {
                    'color': 'red',
                    'fontSize': '0.75em'
                }
            }),
            $eventbus: $({}).on('myCustomEvent', function (event, data) {
                switch (data.name) {
                    case 'init':
                        controller.init();
                        break;
                    case 'cart.get':
                        controller.getCart();
                        break;
                    case 'cart.recieved':
                        controller.getToken(data);
                        break;
                    case 'token.recieved':
                        controller.appendDiv();
                        break;
                    case 'load':
                        controller.loadSDK();
                        break;
                    case 'widget.price':
                        controller.savePrice(data);
                        controller.pub('checkout.updated');
                        break;
                    case 'widget.change':
                        controller.saveSDK(data);
                        controller.pub('checkout.updated');
                        break;
                    case 'checkout.update':
                        controller.updateCheckout(data);
                        break;
                    case 'checkout.updated':
                        controller.updatedCheckout(data);
                        break;
                }
            }),
            appended: false,
            log: function () {
                if (console && !!settings.debugMode) {
                    console.log.apply(console, arguments);
                }
            },
            pub: function (event, data) {
                if (!!settings.debugMode) {
                    console.groupCollapsed(event);
                    console.dir(data);
                    console.groupCollapsed('trace');
                    console.trace();
                    console.groupEnd();
                    console.groupEnd(event);
                }
                this.$eventbus.trigger('myCustomEvent', {
                    'name': event,
                    'data': data
                })
            },
            init: function () {
                var _ = this;
                $('body').on('updated_checkout', function () {
                    _.pub('checkout.updated');
                });
                $.getScript("https://ddelivery.ru/front/widget-cart/public/api.js?" + Math.random(), function () {
                    _.pub('cart.get');
                });
                if (!this.$form) {
                    this.$form = $('form[name=checkout]');
                    this.$form.on('click', '[type="submit"]', function () {
                        return controller.onSubmit();
                    })
                }
            },
            getCart: function () {
                $.get(settings.cart, function (data) {
                    controller.pub('cart.recieved', data)
                })
            },
            getToken: function (data) {
                controller.log(data.data);
                $.post(settings.token, {
                    'products': data.data,
                    'discount': 0,
                    'protocol': window.location.protocol,
                    'processData': false
                }, function (response) {
                    controller.url = response.url;
                    controller.pub('token.recieved', response);
                });
            },
            appendDiv: function () {
                if (this.appended) {
                    this.pub('load');
                    return;
                }
                var $target = $(settings.bindElement).eq(0);
                $target.append($('<div/>', {
                    id: settings.containerId,
                    css: {
                        background: 'transparent',
                        display: 'block',
                        width: '100%',
                        clear: 'both'
                    }
                })).append($('<div/>', {
                    html: this.$errorContainer
                }));
                var inline_style = '#' + settings.containerId + '>iframe{max-width:100%!important;';
                inline_style += '-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}';
                $('head').append($('<style/>', {
                    text: inline_style,
                    type: 'text/css'
                }));
                this.appended = true;
                this.pub('load')
            },
            loadSDK: function () {
                var _ = this;
                var params = settings.ddeliveryParams;
                var urlParams = {};
                urlParams['to_street'] = '';
                urlParams['to_home'] = '';
                urlParams['to_flat'] = '';
                params.url = controller.url + '?' + $.param(urlParams);

                $.get(settings.cart, function (data) {

					var widget = new DDeliveryWidgetCart(settings.containerId, {
							apiScript: "/wp-json/woocommerce-ddelivery/ApiScript",
							products: data,
							discount: 0
						}); 

						// Обработчики событий
						widget.on("change", function (data) {
							_.pub('widget.change', data);
						// Вызовется при изменении выбранного способа доставки
						// или любого другого значения в виджете
						}); 
						widget.on("afterSubmit", function (response) {
                            console.log(response);
                            if (response.status=='ok') {
                                document.getElementById('ddelivery_checker').value='ok';
                            }
						// Вызовется при получении ответа от сервера DDelivery
						}); 
						widget.on("error", function (errors) {
						// Вызовется при возникновении ошибок при обработке запроса,
						// при передаче в виджет некорректных или неполных данных 

						console.error(errors);
					});
                })
            },
            savePrice: function (data) {
                var _ = this;
                $.post(settings.save, data, function (response) {
                    if (response && response.status && response.status === 'ok') {
                        _.pub('checkout.update');
                    } else {
                        _.pub('error');
                    }
                });
            },
            updateCheckout: function (data) {
                /**
                 * @see woocommerce/assets/js/frontend/checkout.js
                 */
                $('body').trigger('update_checkout');
            },
            updatedCheckout: function () {
                if (typeof DDeliveryModule !== 'undefined') {
                    if (DDeliveryModule.validate()) {
                        controller.log('validating ddelivery success');
                        this.$errorContainer.text('').parent().hide();
                    } else {
                        controller.log('validating ddelivery failed');
                        this.$errorContainer.text(DDeliveryModule.getErrorMsg()).parent().show();
                    }
                }
            },
            debug: function (anything) {
                $.post(settings.debug, anything, function (response) {
                    controller.log(response);
                })
            },
            saveSDK: debounce(function (data) {
                $.post(settings.saveSDK, data.data, function (response) {
                    controller.log(response);
                    $('body').trigger('update_checkout');
                })                
            }, 1000),
            onSubmit: function () {
                // default checkout template has #shipping_method block and each shipping is radio-input
                var $shippingMethods = $('input.shipping_method');
                var $currentMethod;
                if($shippingMethods.parents().filter('#shipping_method').length){
                    // multiple delivery methods - radio inputs
                    $currentMethod = $shippingMethods.filter('[checked=checked]');
                }else{
                    // single delivery - hidden input
                    $currentMethod = $shippingMethods;
                }
                if($currentMethod.length && 'ddelivery-id'!=$currentMethod.eq(0).val()){
                    return true;
                }
                DDeliveryModule.sendForm({
                    success: function () {
                        controller.log('submit success');
                        controller.$errorContainer.text('').parent().hide();
                        controller.$form.submit();
                    },
                    error: function () {
                        controller.log('submit prevented');
                        if (settings.debugMode) {
                            console.trace(DDeliveryModule.getErrorMsg());
                        }
                        controller.$errorContainer.text(DDeliveryModule.getErrorMsg()).parent().show();
                        $('html, body').animate({
                            scrollTop: ( $('#' + settings.containerId).offset().top  )
                        }, 1000);
                    }
                });
                return false;
            }
        };
        controller.pub('init');
    })
})(window.jQuery);