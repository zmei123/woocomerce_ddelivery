<?php
$curl = function ($url) {
    $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
    curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
    curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа

    $content = curl_exec($ch);
    curl_close($ch);

    return $content;
};

if ($_GET['_route_'] == 'statuses.json') {
    echo $curl($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/wp-json/woocommerce-ddelivery/getstatuses');
} elseif ($_GET['_route_'] == 'payment-methods.json') {
    echo $curl($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/wp-json/woocommerce-ddelivery/getpayment');
} elseif ($_GET['_route_'] == 'traffic-orders.json') {
    if (isset($_POST['id']) && isset($_POST['status_cms']))
    {
        $curl($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/wp-json/woocommerce-ddelivery/updatestatus?ddelivery_id='.$_POST['id'].'&status='.$_POST['status_cms'].'&ddelivery_track_number='.$_POST['track_number']);
    }
    $fp = fopen('data.log', 'a');
    fwrite($fp, date('r').' '.json_encode($_POST).' '.json_encode($_GET)."\n".$_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/wp-json/woocommerce-ddelivery/updatestatus?ddelivery_id='.$_POST['id'].'&status='.$_POST['status_cms'].'&ddelivery_track_number='.$_POST['track_number']."\n");
    fclose($fp);    
}